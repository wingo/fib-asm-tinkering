const fs = require('fs');
var source = fs.readFileSync('./fib.wasm');
var buffer = new Uint8Array(source);
const env = {
    memoryBase: 0,
    tableBase: 0,
    memory: new WebAssembly.Memory({ initial: 1024 }),
    table: new WebAssembly.Table({ initial: 0, element: 'anyfunc' })
}

WebAssembly.instantiate(buffer, {env:env}).then(
    ({module, instance}) =>
        console.log(instance.exports.fib(40)));
