#define JIT_V0 %rbx
#define JIT_V1 %r12
#define JIT_V2 %r13
#define JIT_R0 %rax
#define JIT_R1 %rcx
#define JIT_R2 %rdx
#define JIT_TMP0 %r11
	
#define T0 JIT_V1
#define T1 JIT_V2
#define T2 JIT_R2
#define THREAD JIT_V0
#define SP JIT_R0
#define FP JIT_R1
#define LR JIT_TMP0

#define THREAD_IP 8
#define THREAD_SP 16
#define THREAD_FP 24
#define THREAD_SP_MIN_SINCE_GC 32
#define THREAD_STACK_LIMIT 40
#define THREAD_PENDING_ASYNCS 0x90
#define THREAD_BLOCK_ASYNCS 0x98

#define EOL $0x304
#define FIXNUM_TAG $0x2
#define FIXNUM_1 $0x4
#define FIXNUM_2 $0x8

str:
        .global main

error_wrong_num_args:
        int3

thread_expand_stack:
        int3

less_p:
        int3

sub_immediate:
        int3

error_no_values:
        int3

scm_sum:
        int3

handle_interrupts_trampoline:
        int3

exit_mcode:
        int3

        .align 16
fib:
   /* 2    (assert-nargs-ee/locals 1 5)    ;; 6 slots (0 args) */
	mov    FP,T0
	sub    SP,T0
	cmp    $0x8,T0
	je     args_ok
	mov    $0x108d2ac,T0
	mov    T0,THREAD_IP(THREAD)
	mov    THREAD,%rdi
	call   error_wrong_num_args
args_ok:
	lea    -0x30(FP),SP
	mov    THREAD_SP_MIN_SINCE_GC(THREAD),T0
	cmp    T0,SP
	jge    store_sp
	mov    THREAD_STACK_LIMIT(THREAD),T0
	cmp    T0,SP
	jge    update_min_since_gc
	mov    $0x108d2ac,T0
	mov    T0,THREAD_IP(THREAD)
	mov    THREAD,%rdi
	mov    SP,%rsi
	call   thread_expand_stack
	mov    THREAD_SP(THREAD),SP
	mov    THREAD_FP(THREAD),FP
	jmp    i3
update_min_since_gc:    
	mov    SP,THREAD_SP_MIN_SINCE_GC(THREAD)
store_sp:    
	mov    SP,THREAD_SP(THREAD)
i3:
   /* 3    (make-short-immediate 4 10)     ;; 2 */
	mov    $0xa,T0
	mov    T0,0x20(SP)
i4:
   /* 4    (<? 5 4) */
   /* 5    (jl 22)                         ;; -> L1 */
	mov    0x28(SP),T0
	mov    0x20(SP),T1
	mov    T0,T2
	and    T1,T2
	test   FIXNUM_TAG,T2
	jne    less_than_fast
	mov    $0x108d2b4,T2
	mov    T2,THREAD_IP(THREAD)
	mov    T0,%rdi
	mov    T1,%rsi
	call   less_p
	mov    %rax,T0
	mov    THREAD_SP(THREAD),SP
	cmp    $0x2,T0
	je     L1
	jmp    i6
less_than_fast:
	cmp    T1,T0
	jl     L1
i6:
/* 6    (call-scm<-scm-uimm 1 5 1 3) */
	mov    0x28(SP),T0
	test   FIXNUM_TAG,T0
	je     sub1_slow
	sub    FIXNUM_1,T0
	jno    store_sub1_result
	add    FIXNUM_1,T0
sub1_slow:      
	mov    $0x108d2bc,T1
	mov    T1,THREAD_IP(THREAD)
	mov    T0,%rdi
	mov    $0x1,%esi
	call   sub_immediate
	mov    %rax,T0
	mov    THREAD_SP(THREAD),SP
store_sub1_result:     
	mov    T0,0x8(SP)
i8:     
/* 8    (handle-interrupts) */
	lea    THREAD_PENDING_ASYNCS(THREAD),T0
	mov    (T0),T0
	cmp    EOL,T0
	je     i9
	movslq THREAD_BLOCK_ASYNCS(THREAD),T0
	test   T0,T0
	jne    i9
	mov    $0x108d2c4,T0
	mov    T0,THREAD_IP(THREAD)
	call   handle_interrupts_trampoline
	jmp    i8
i9:     
/* 9    (call-label 4 1 -9)             ;; fib at #x108d2a4 */
	jmp    i9_push_frame
i9_tramp:
	pop    LR
	mov    LR,(FP)
        jmp    fib
i9_push_frame:
	mov    THREAD_FP(THREAD),FP
	sub    $0x20,FP
	mov    $0x108d2d4,T0
	mov    T0,0x8(FP)
	mov    $0x4,T0
	mov    T0,0x10(FP)
	mov    FP,THREAD_FP(THREAD)
	lea    -0x8(FP),SP
	mov    SP,THREAD_SP(THREAD)
	call   i9_tramp
i12:    
/* 12    (receive 1 4 6)                  */
	mov    FP,T0
	sub    SP,T0
	cmp    $0x20,T0
	jg     return1_ok
	mov    $0x108d2d4,T0
	mov    T0,THREAD_IP(THREAD)
	call   error_no_values
return1_ok:
	mov    -0x28(FP),T0
	mov    T0,-0x10(FP)
	lea    -0x30(FP),SP
	mov    SP,THREAD_SP(THREAD)
i14:    
/* 14    (call-scm<-scm-uimm 0 5 2 3) */
	mov    0x28(SP),T0
	test   FIXNUM_TAG,T0
	je     sub2_slow
	sub    FIXNUM_2,T0
	jno    store_sub2_result
	add    FIXNUM_2,T0
sub2_slow:
	mov    $0x108d2dc,T1
	mov    T1,THREAD_IP(THREAD)
	mov    T0,%rdi
	mov    $0x2,%esi
	call   sub_immediate
	mov    %rax,T0
	mov    THREAD_SP(THREAD),SP
store_sub2_result:      
	mov    T0,(SP)
i16:    
/* 16    (handle-interrupts) */
	lea    THREAD_PENDING_ASYNCS(THREAD),T0
	mov    (T0),T0
	cmp    EOL,T0
	je     i17
	movslq THREAD_BLOCK_ASYNCS(THREAD),T0
	test   T0,T0
	jne    i17
	mov    $0x108d2e4,T0
	mov    T0,THREAD_IP(THREAD)
	call   handle_interrupts_trampoline
	jmp    i16
i17:    
/* 17    (call-label 5 1 -17)            ;; fib at #x108d2a4 */
	jmp    i17_push_frame
i17_tramp:
        pop    LR
        mov    LR,(FP)
        jmp    fib
i17_push_frame:
	mov    THREAD_FP(THREAD),FP
	sub    $0x28,FP
	mov    $0x108d2f4,T0
	mov    T0,0x8(FP)
	mov    $0x5,T0
	mov    T0,0x10(FP)
	mov    FP,THREAD_FP(THREAD)
	lea    -0x8(FP),SP
	mov    SP,THREAD_SP(THREAD)
	call   i17_tramp
i20:    
/* 20    (receive 0 5 6)                  */
	mov    FP,T0
	sub    SP,T0
	cmp    $0x28,T0
	jg     return2_ok
	mov    $0x108d2f4,T0
	mov    T0,THREAD_IP(THREAD)
	call   error_no_values
return2_ok:
	mov    -0x30(FP),T0
	mov    T0,-0x8(FP)
	lea    -0x30(FP),SP
	mov    SP,THREAD_SP(THREAD)
i22:    
/* 22    (call-scm<-scm-scm 5 4 5 0) */
	mov    0x20(SP),T0
	mov    0x28(SP),T1
	test   FIXNUM_TAG,T0
	je     sum_slow
	test   FIXNUM_TAG,T1
	je     sum_slow
	sub    FIXNUM_TAG,T0
	add    T1,T0
	jno    store_sum_result
	sub    T1,T0
	add    FIXNUM_TAG,T0
sum_slow:       
	mov    $0x108d2fc,T2
	mov    T2,THREAD_IP(THREAD)
	mov    T0,%rdi
	mov    T1,%rsi
	call   scm_sum
	mov    %rax,T0
	mov    THREAD_SP(THREAD),SP
store_sum_result:       
	mov    T0,0x28(SP)
i24:    
/* 24    (reset-frame 1)                 ;; 1 slot */
	mov    THREAD_FP(THREAD),FP
	lea    -0x8(FP),SP
	mov    SP,THREAD_SP(THREAD)
i25:    
/* 25    (handle-interrupts)              */
	lea    THREAD_PENDING_ASYNCS(THREAD),T0
	mov    (T0),T0
	cmp    EOL,T0
	je     i26
	movslq THREAD_BLOCK_ASYNCS(THREAD),T0
	test   T0,T0
	jne    i26
	mov    $0x108d308,T0
	mov    T0,THREAD_IP(THREAD)
	call   handle_interrupts_trampoline
	jmp    i25
i26:    
/* 26    (return-values)                  */
	mov    THREAD_FP(THREAD),T0
	mov    0x10(T0),FP
	lea    0x0(,FP,8),FP
	add    T0,FP
	mov    FP,THREAD_FP(THREAD)
	mov    (T0),LR
	push   LR
	ret

/* L1: */
L1:     
i27:    
/* 27    (reset-frame 1)                 ;; 1 slot */
	mov    THREAD_FP(THREAD),FP
	lea    -0x8(FP),SP
	mov    SP,THREAD_SP(THREAD)
i28:    
/* 28    (handle-interrupts)              */
	lea    THREAD_PENDING_ASYNCS(THREAD),T0
	mov    (T0),T0
	cmp    EOL,T0
	je     i29
	movslq THREAD_BLOCK_ASYNCS(THREAD),T0
	test   T0,T0
	jne    i29
	mov    $0x108d314,T0
	mov    T0,THREAD_IP(THREAD)
	call   handle_interrupts_trampoline
	ret
i29:    
/* 29    (return-values)                  */
	mov    THREAD_FP(THREAD),T0
	mov    0x10(T0),FP
	lea    0x0(,FP,8),FP
	add    T0,FP
	mov    FP,THREAD_FP(THREAD)
	mov    (T0),LR
        push   LR
	ret

usage_orig_N:
        int3

main:
	/* Align stack. */
	sub $8, %rsp
	cmp $2, %rdi
	jne usage_orig_N
	movq 8(%rsi), %rdi
	call atoll@PLT
	mov %rax, %r9
	shl $2, %r9
        add $2, %r9
	lea stack_top(%rip), SP
        sub  $0x20, SP
	movq $0, 0x18(SP) /* dl */
	movq $0, 0x10(SP) /* vra */
	lea return_successfully(%rip),%r8
        mov %r8,0x08(SP) /* mra */
	lea 0x8(SP), FP
	movq %r9, 0x0(SP) /* argv[0] */
	lea thread(%rip), THREAD
        movq $0, THREAD_IP(THREAD)
        mov SP, THREAD_SP(THREAD)
        mov FP, THREAD_FP(THREAD)
        mov SP, THREAD_SP_MIN_SINCE_GC(THREAD)
	lea stack_bottom(%rip), %r8
        movq %r8, THREAD_STACK_LIMIT(THREAD)
        movq EOL, THREAD_PENDING_ASYNCS(THREAD)
	jmp fib

return_successfully:
	movq 0(SP), %rsi
	shr $2, %rsi
	lea format(%rip), %rdi
	xor %eax,%eax
	call printf@PLT
	add $8, %rsp
	
        mov $0, %rdi
        mov $60, %rax
        syscall	

.data
thread:
        .zero 0x130
stack_bottom:
        .zero 1000000
stack_top:
format: .asciz "Result: %llu\n"
