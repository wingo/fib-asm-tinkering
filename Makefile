
SOURCES=$(wildcard *.S)
TARGETS=$(basename $(SOURCES))

all: $(TARGETS)

test: all
	@for i in $(TARGETS); do echo "Testing $$i:"; time ./$$i 40; done

%: %.S
	$(CC) -o $@ $<

clean:
	rm -f $(TARGETS)

hs-fib: fib.hs
	guix environment --ad-hoc ghc -- ghc -o hs-fib fib.hs

hs-fib-opt: fib.hs
	guix environment --ad-hoc ghc -- ghc -O -o hs-fib-opt fib.hs

gcc-fib-O0: fib.c
	guix environment --ad-hoc gcc-toolchain -- gcc -o $@ -O0 $<

gcc-fib-O1: fib.c
	guix environment --ad-hoc gcc-toolchain -- gcc -o $@ -O1 $<

gcc-fib-O3: fib.c
	guix environment --ad-hoc gcc-toolchain -- gcc -o $@ -O3 $<

clang-fib-O0: fib.c
	guix environment --ad-hoc clang -- clang -o $@ -O0 $<

clang-fib-O1: fib.c
	guix environment --ad-hoc clang -- clang -o $@ -O1 $<

clang-fib-O3: fib.c
	guix environment --ad-hoc clang -- clang -o $@ -O3 $<

chez-fib: fib.scm
	echo '(compile-file "fib.scm")' | guix environment --ad-hoc chez-scheme -- scheme -q

chez-fib2: fib2.scm
	echo '(compile-file "fib2.scm")' | guix environment --ad-hoc chez-scheme -- scheme -q


# Run as "echo '(exit)' | scheme -q fib.so"

# guile

.PHONY: test clean all
