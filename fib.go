package main
 
import "os"
import "fmt"
import "strconv"
 
func fib(n int) int {
    if n < 2 {
        return n;
    } else {
        return fib(n-1) + fib(n-2)
    }
}

func main() {
    n, _ := strconv.Atoi(os.Args[1])
    
    fmt.Println(fib(n))
}
