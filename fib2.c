#include <stdio.h>
#include <stdlib.h>

static long fib(long n)
{
  if (n < 2) return n;
  return fib(n - 1) + fib(n - 2);
}

int main (int argc, char *argv[])
{
  if (argc != 2)
    return 1;
  printf("Result: %lu\n", fib(atoll(argv[1])));
  return 0;
}
